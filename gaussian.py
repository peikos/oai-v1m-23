import numpy as np
import matplotlib.pyplot as plt

num_sims = 10000

sims = []
for _ in range(num_sims):
    heads = sum(1 if r > 0.5 else 0 for r in np.random.random(1000))
    sims.append(heads)

plt.hist(sims, bins=20)

plt.show()
