
import math

# print(math.sqrt(2))

def floor(real):
    i = int(real)
    if i > real:     # Het getal is naar boven afgerond, want blijkbaar negatief.
        return i - 1
    else:
        return i

# print(f"0.3 als float: {0.1+0.1+0.1}")

# print(floor(3.14159), floor(-3.14159), floor(2.0))

def log2(x):
    return math.log(x) / math.log(2)

# print(log2(5)) # Klopt met de slides

# TODO
def log(x, base=2):
    pass # TODO

# Hier schrijven we de functie
def linear_search(lst, target):
    for item in lst:
        if item == target:
            return True
    return False

# Hier roepen we de functie aan

# my_list = [1, 2, 2, 6, 3, 2, 5]
#
# print("Linear search")
# if linear_search(my_list, 3):
#     print("Zit er in")
# else:
#     print("Nee!")


# Binary Search

def binary_search(lst, e):
    left = 0
    right = len(lst)-1

    while left <= right:

        i = (left+right) // 2

        if lst[i] < e:
            left = i+1

        elif lst[i] > e:
            right = i-1

        else:
            return True
    return False


# Aanroep

# my_list = [1, 2, 2, 6, 3, 2, 5]
#
# print("Binary search (unsorted)")
# if binary_search(my_list, 3):
#     print("Zit er in")
# else:
#     print("Nee!")
#
# my_list.sort()
#
# print("Binary search (sorted)")
# if binary_search(my_list, 3):
#     print("Zit er in")
# else:
#     print("Nee!")
#
# print("----------")

def swap(lst, a, b):
    """
    Swap twee items in een lijst.
    :param lst: Lijst met items
    :param a:   Index van het ene te swappen item
    :param b:   Index van het andere te swappen item
    :return:
    """
    print("Swapping index", a, "and", b)
    # temp = lst[a]
    # lst[a] = lst[b]
    # lst[b] = temp

    lst[a], lst[b] = lst[b], lst[a]

def find_minimum(lst, start):
    """
    Vind het volgende item om te swappen.
    :param lst: De totale lijst met items
    :param start:  De start index van het nog niet gesorteerde deel
    :return: Index van het kleinste item in het niet-gesorteerde deel
    """
    index_minimum = start
    for index_current in range(start+1, len(lst)):
        if lst[index_current] < lst[index_minimum]:
            index_minimum = index_current
    return index_minimum

def selection_sort(lst):
    """
    Sorteer een lijst met Selection Sort.
    :param lst: Een (ongesorteerde) lijst
    :return:    Een gesorteerde kopie van de lijst
    """
    sorted_lst = lst.copy()
    for index in range(len(sorted_lst)):
        min_index = find_minimum(sorted_lst, index)
        swap(sorted_lst, index, min_index)
        print("List is now:", sorted_lst)
    return sorted_lst


# lst = [5,3,1,2,4]
# print("List is:", lst)
#
# sorted_list = selection_sort(lst)
#
# print("Sorted:", sorted_list)


def fun(x, y):
    print(f"x is nu {x} en y is nu {y}")
    if x == 1:
        return y
    else:
        return y + fun(x-1, y)

# print(fun(4,3))

def faculteit_loop(n):
    resultaat = 1
    for i in range(1, n+1):
        resultaat = resultaat * i
        # resultaat *= i
    return resultaat

def faculteit_rec(n):
    if n == 0:
        return 1
    else:
        return n * faculteit_rec(n-1)
    # Base case? 0! = 1
    # Recursiestap? n! = n * (n-1)!



# getal = input("Waar wil je de faculteit van weten?")
# print(f"{getal}! = {faculteit_loop(int(getal))}")

print(faculteit_rec(5))