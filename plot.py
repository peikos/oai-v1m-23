# Plot Library (pip install matplotlib)
import matplotlib.pyplot as plt

# Data van de oefening
x = [1,2,3,4]
y = [4,7,8,15]

# Nodig om de lijnen toe te voegen
fig, ax = plt.subplots()

# Plot de datapunten
plt.scatter(x, y)

# Plot twee lijnen
ax.axline((0, 4), slope=3., color='C0', label='by slope')
ax.axline((0, 3), slope=4., color='C1', label='by slope')

# Toon het resultaat
plt.show()
