# Orientatie op AI &mdash; V1M 2023

In deze repo bewaar ik de laatste versie van de code die ik in de klas heb behandeld.

## Changelog
- Versie 0: Voorbeeld-code eerste twee lessen (getallen)
- Versie 1: Linear search
- Versie 2: Binary search en selection sort
